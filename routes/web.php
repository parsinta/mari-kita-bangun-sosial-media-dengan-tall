<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('timeline');
})->name('home');

Route::middleware('auth')->group(function() {
    Route::get('timeline', 'TimelineController')->name('timeline');
    Route::livewire('status/{hash}/edit', 'status.edit')->layout('layouts.app')->name('status.edit');
    Route::livewire('status/{hash}/delete', 'status.delete')->layout('layouts.modal')->name('status.delete');
});

Route::livewire('settings', 'account.edit')->layout('layouts.app', ['title' => 'Settings'])->name('settings')->middleware('auth');
Route::livewire('user/{identifier}', 'account.show')->layout('layouts.app')->name('account.show');
Route::get("user/{identifier}/{follow}", "FollowingController")->name('account.following');

Route::livewire('status/{hash}', 'status.show')->layout('layouts.app')->name('status.show');

Route::middleware('guest')->group(function () {
    Route::view('login', 'auth.login')->name('login');
    Route::view('register', 'auth.register')->name('register');
});

Route::view('password/reset', 'auth.passwords.email')->name('password.request');
Route::get('password/reset/{token}', 'Auth\PasswordResetController')->name('password.reset');

Route::middleware('auth')->group(function () {
    Route::view('email/verify', 'auth.verify')->middleware('throttle:6,1')->name('verification.notice');
    Route::get('email/verify/{id}/{hash}', 'Auth\EmailVerificationController')->middleware('signed')->name('verification.verify');

    Route::post('logout', 'Auth\LogoutController')->name('logout');

    Route::view('password/confirm', 'auth.passwords.confirm')->name('password.confirm');
});

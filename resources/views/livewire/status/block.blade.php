<div class="flex w-full" wire:poll.750ms>
    <div class="flex-shrink-0 mr-3">
        <a href="{{ route('account.show', ['identifier' => $status->user->usernameOrHash]) }}">
            <img class="w-16 h-16 rounded-full object-cover object-center" src="{{ $status->user->gravatar() }}">
        </a>
    </div>
    <div class="w-full relative">
        <div class="flex justify-between" x-data="{ dropdownIsOpen: false }">
            <a href="{{ route('account.show', ['identifier' => $status->user->usernameOrHash]) }}" class="font-semibold text-cool-gray-900 hover:underline">{{ $status->user->name }}</a>
            @can('update', $status)
                <button @click="dropdownIsOpen = !dropdownIsOpen" class="hover:bg-cool-gray-100 p-1 rounded-full focus:outline-none">
                    <svg class="w-5 h-5" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                    </svg>
                </button>
                <div :class="{ 'hidden': !dropdownIsOpen }" class="text-sm bg-white border border-cool-gray-200 rounded py-2 w-48 absolute right-0 top-0 mt-8">
                    <a href="{{ route('status.edit', $status->hash) }}" class="block px-3 py-1 hover:bg-cool-gray-100 text-cool-gray-600 hover:text-cool-gray-800">Edit the status</a>
                    <a href="{{ route('status.delete', $status->hash) }}" class="block px-3 py-1 hover:bg-cool-gray-100 text-cool-gray-600 hover:text-cool-gray-800">Remove the status</a>
                </div>
            @endcan
        </div>
        <a href="{{ route('status.show', $status->hash) }}">
            <div class="text-sm text-cool-gray-400 mb-3">{{ $status->published }}</div>
            <div class="text-cool-gray-700 leading-relaxed">{!! nl2br($status->body) !!}</div>
            <div class="text-sm text-cool-gray-400 mt-4 flex items-center -mx-4">
                <div class="flex items-center mx-4">
                    <svg class="w-5 h-5 mr-3" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"></path>
                    </svg>
                    <a href="#showReplyForm">
                        {{ $status->comments_count }} {{ Str::plural('Comment', $status->comments_count) }}
                    </a>
                </div>
                <livewire:status.like :key="$status->id" :status="$status"/>
            </div>
        </a>
    </div>
</div>
